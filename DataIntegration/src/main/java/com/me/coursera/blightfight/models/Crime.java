package com.me.coursera.blightfight.models;

import java.util.Date;

/**
 * Created by sajit on 6/11/16.
 */
public class Crime {
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    private String date;

    private String category;

    private long buildingId;

    public Crime(String date, long buildingId, String category) {
        this.date = date;
        this.buildingId = buildingId;
        this.category = category;
    }

    public Crime(String date,String category){
        this.date = date;
        this.category = category;
    }

    public void setBuildingId(long buildingId) {
        this.buildingId = buildingId;
    }

    public long getBuildingId() {
        return buildingId;
    }
}
