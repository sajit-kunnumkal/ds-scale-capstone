package com.me.coursera.blightfight.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sajit on 6/11/16.
 */
public class BlightViolation {

    private String code;
    private long buildingId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    private String date;

    public long getBuildingId() {
        return buildingId;
    }

    public BlightViolation(String code,String date,long buildingId){
        this.code = code;
        this.buildingId = buildingId;
        this.date = date;
    }
    public BlightViolation(String code,String date){
        this.code = code;
        this.date = date;
    }

}
