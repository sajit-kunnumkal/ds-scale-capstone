package com.me.coursera.blightfight.models;

import java.util.Date;

/**
 * Created by sajit on 6/19/16.
 */
public class Complain311 {
    private String issueType;
    private String ticketStatus;
    private long buildingId;

    public Complain311(String issueType, String ticketStatus, long buildingId, String date) {
        this.issueType = issueType;
        this.ticketStatus = ticketStatus;
        this.buildingId = buildingId;
        this.date = date;
    }

    public String getDate() {

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(long buildingId) {
        this.buildingId = buildingId;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    private String date;
}
