package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.Building;
import com.me.coursera.blightfight.models.Complain311;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajit on 6/30/16.
 */
public class T11Util {

    public static void main(String[] args) throws IOException {
        int count = 433558;
        String permitFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit311.csv";

        Reader reader = new FileReader(permitFile);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);

        List<String> output = new ArrayList<>();
        output.add(BV.HEADER);
        for(CSVRecord record : parser){

            double latitude = 0.0,longitude = 0.0;

            String date = record.get("ticket_created_date_time");
            String category = record.get("issue_type");
            String location = null;
            try{

                latitude = Double.valueOf(record.get("lat"));
                longitude = Double.valueOf(record.get("lng"));
                location = "("+latitude+":"+longitude+")";

            }catch (NumberFormatException nfe){
                //go on
                //lets try location
                String[] addresspieces = record.get("location").split("\n");
                if(addresspieces.length >=2){
                    location = BVUtils.extractLocation(addresspieces[1]);


                }




            }
            if(location != null){
                output.add(count+","+location+","+category+","+date);
                count++;
            }


        }
        File oFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/t11-data.csv");
        FileUtils.writeLines(oFile,output);

    }
}
