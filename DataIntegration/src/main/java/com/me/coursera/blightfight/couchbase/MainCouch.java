package com.me.coursera.blightfight.couchbase;

import com.couchbase.client.deps.io.netty.util.internal.StringUtil;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.google.common.collect.Lists;
import com.sun.org.apache.bcel.internal.generic.LXOR;
import org.apache.commons.lang3.RandomStringUtils;
import rx.Observable;
import rx.functions.Func1;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by sajit on 6/22/16.
 */
public class MainCouch {

    public static void main(String[] args) {
        Cluster cluster = CouchbaseCluster.create();
        Bucket bucket = cluster.openBucket();
        int size = 22000;
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {

            //JsonDocument doc = JsonDocument.create("k"+i, JsonObject.empty().put("k"+i,i));
            //bucket.upsert(doc);
            arr[i] = "k" + i;
        }

        System.out.println("inserted  " + size);
        System.out.println(new Timestamp((new Date()).getTime()));

        List<JsonDocument> foundDocs = Observable.from(arr)

                .flatMap(new Func1<String, Observable<JsonDocument>>() {
                    @Override
                    public Observable<JsonDocument> call(String id) {
                        return bucket.async().get(id);
                    }
                })
                .toList()
                .toBlocking()
                .single();

        System.out.println("Help " + foundDocs.size() + ":" );
        System.out.println(new Timestamp((new Date()).getTime()));
    }


}
