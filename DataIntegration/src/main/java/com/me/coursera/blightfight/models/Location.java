package com.me.coursera.blightfight.models;

/**
 * Created by sajit on 6/28/16.
 */
public class Location {

    final String lat;
    final String lng;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (lat != null ? !lat.equals(location.lat) : location.lat != null) return false;
        return lng != null ? lng.equals(location.lng) : location.lng == null;

    }

    @Override
    public int hashCode() {
        int result = lat != null ? lat.hashCode() : 0;
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        return result;
    }

    public Location(String lat, String lng){
        this.lat = lat;
        this.lng = lng;

    }

    public String getLat(){
        return  this.lat;
    }

    public String getLng(){
        return  this.lng;
    }

    public static Location from(String s){

        String[] tokens = s.split(":");
        return new Location(tokens[0].substring(1),tokens[1].substring(0,tokens[1].length()-2));
    }
}
