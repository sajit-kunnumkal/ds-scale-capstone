package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.Location;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by sajit on 7/3/16.
 */
public class ModelTrain1 {
    enum Types {
        CRIME,BV,T11
    }
    public static void main(String[] args) throws IOException {
        File file = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/bbld.csv");
        List<String> lines = FileUtils.readLines(file);
        Map<Location,Counts> countsMap = new HashMap<>();
        Set<Location> blighted = new HashSet<>();
        for(int i=1;i<lines.size();i++){
            String record = lines.get(i);
            String[] tokens = record.split(",");
            Location location = new Location(tokens[1],tokens[2]);
            Counts counts = countsMap.get(location);
            if(counts == null){
                counts = new Counts();
            }
            //System.out.println(record);
            try{
                long id = Long.valueOf(tokens[0]);
                Types type = mapId(id);
                counts.increment(type);
                countsMap.put(location,counts);
                if(Integer.valueOf(tokens[5])==1){
                    blighted.add(location);
                }
            }catch (NumberFormatException nfe){
                //
            }


        }
        List<String> records = new ArrayList<>();
        List<String> blightedRecords = new ArrayList<>();
        List<String> nonBlightedRecords = new ArrayList<>();
        for(Location location : countsMap.keySet()){
            Counts counts = countsMap.get(location);

            int isBlight = blighted.contains(location)? 1 : 0;
            double blightedNeighborsRatio = calculate(location,countsMap.keySet(),blighted);
            //System.out.println(blightedNeighborsRatio+"Ratio ");
            String record = location.getLat()+","+location.getLng()+","+counts.bvCount+","+counts.crimeCount+","+counts.t11Count+","+blightedNeighborsRatio+","+isBlight;
            if(isBlight==1){

                blightedRecords.add(record);
            }
            else{
                nonBlightedRecords.add(record);
            }
            records.add(record);
            System.out.println(record);
        }
        File file1 = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/train-1.csv");
        //Collections.shuffle(nonBlightedRecords);
        //List<String> randomSubset = nonBlightedRecords.subList(0,4641);
        FileUtils.writeLines(file1,records);

        //File file2 = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/train-2.csv");
        //List<String> outputLines = new ArrayList<>(blightedRecords);
        //outputLines.addAll(randomSubset);
        //System.out.println(outputLines.size());
        //FileUtils.writeLines(file2,outputLines);


    }

    public static double calculate(Location currentLocation, Set<Location> allLocations,Set<Location> blightedLocations) {

        int blightedCount = 0,totalCount=0;
        for(Location location : allLocations){
            if(!location.equals(currentLocation)){
                double dist = GeoUtils.distance(Double.valueOf(location.getLat()),
                        Double.valueOf(location.getLng()),
                        Double.valueOf(currentLocation.getLat()),
                        Double.valueOf(currentLocation.getLng()));
                if(dist < 500){
                    if(blightedLocations.contains(location)){
                        blightedCount++;
                    }
                    totalCount++;
                }
            }
        }
        if(totalCount==0){
            return 0.0;
        }
        double ratio =  (double)blightedCount/(double)totalCount;
        double roundOff = Math.round(ratio * 1000.0) / 1000.0;
        return roundOff;
    }

    private static Types mapId(long id) {
        if(id <= 307804){
            return Types.BV;
        }
        if(id>307804 && id<= 427620){
            return Types.CRIME;
        }
        if(id>=433587 && id<= 453266 ){
            return Types.T11;
        }
        return null;
    }
}
class Counts{
    public int bvCount = 0;
    public int crimeCount = 0;
    public int t11Count = 0;

    public void increment(ModelTrain1.Types types){
        if(types == ModelTrain1.Types.CRIME){
            crimeCount++;
        }
        if(types == ModelTrain1.Types.BV){
            bvCount++;
        }
        if(types == ModelTrain1.Types.T11){
            t11Count++;
        }
    }
}
