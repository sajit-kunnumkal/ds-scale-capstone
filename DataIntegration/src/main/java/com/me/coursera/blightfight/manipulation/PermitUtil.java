package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.Building;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajit on 6/30/16.
 */
public class PermitUtil {

    public static void main(String[] args) throws IOException {
        int count = 427592;
        String permitFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/permits-alldata.csv";

        Reader reader = new FileReader(permitFile);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);

        List<String> output = new ArrayList<>();
        output.add(BV.HEADER);


        for(CSVRecord record : parser){


            String[] addresspieces = record.get("site_location").split("\n");
            if (addresspieces.length >= 3) {
                String location = BVUtils.extractLocation(addresspieces[2]);
                String date = record.get("PERMIT_ISSUED");
                String category = record.get("BLD_PERMIT_TYPE");
                output.add(count+","+location+","+category+","+date);
                count++;
            }






        }

        File oFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/permit-data.csv");
        FileUtils.writeLines(oFile,output);
    }
}
