package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.Building;
import com.me.coursera.blightfight.models.Crime;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajit on 6/29/16.
 */
public class CrimeUtilz {
    public static void main(String[] args) throws IOException {
        int count = 307805;
        String crimeFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit-crime.csv";

        Reader reader = new FileReader(crimeFile);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);

        List<String> output = new ArrayList<>();
        output.add(BV.HEADER);


        for(CSVRecord record : parser){
            String date = record.get("INCIDENTDATE");
            String category = record.get("CATEGORY");
            double latitude = 0.0,longitude = 0.0;

            try{

                latitude = Double.valueOf(record.get("LAT"));
                longitude = Double.valueOf(record.get("LON"));



            }catch (NumberFormatException nfe){
                //go on
                //lets try location
                String[] addresspieces = record.get("LOCATION").split("\n");


                try{
                    Pair<Double,Double> location = new ImmutablePair<>(0.0,0.0); //BVUtils.extractLocation(addresspieces[1]);
                    latitude = location.getLeft().doubleValue();
                    longitude = location.getRight().doubleValue();

                }catch (Exception e){
                   //
                }



            }
            String location = "("+latitude+":"+longitude+")";
            if(Double.compare(latitude,longitude) != 0){
                output.add(count+","+location+","+category+","+date);
                count++;

            }


        }

        File oFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/crime-data.csv");
        FileUtils.writeLines(oFile,output);
    }
}
