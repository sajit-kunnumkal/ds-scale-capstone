package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.util.*;

/**
 * Created by sajit on 6/17/16.
 */
public class BVUtils {

    public static String DIR = "/home/sajit/coursera/ds_scale/blightfight/course-data/";
    static List<Building> bvBuildings = new ArrayList<Building>();
    static List<Building> pBlds = new ArrayList<Building>();
    static List<Building> three11Blds = new ArrayList<Building>();
    static List<Building> cBlds = new ArrayList<Building>();
    public static final String NEW_LINE_SEPARATOR = "\n";

    /**
     * 1. Determine data problems Identify Violation Addresses which have different Violation Street Name & Number
     *
     */
//    private static void createBVBuildings(int startCount) throws IOException {
//
//        String blightFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit-blight-violations.csv";
//        Reader reader = new FileReader(blightFile);
//        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);
//        int count = startCount;
//        System.out.println("Count at start BV" + count);
//        List<BlightViolation> blightViolations = new ArrayList<>();
//        for(CSVRecord record : parser){
//            String[] addressParts = record.get("ViolationAddress").split("\n");
//            try{
//                Pair<Double,Double> location = extractLocation(addressParts[2]);
//                Building bld = new Building(count,location.getLeft(),location.getRight(),addressParts[0].toLowerCase());
//                bvBuildings.add(bld);
//                BlightViolation blightViolation = new BlightViolation(record.get("ViolationCode"),record.get("TicketIssuedDT"),count);
//                blightViolations.add(blightViolation);
//                count++;
//            }catch (Exception e){
//                //nothing to do
//            }
//
//
//
//        }
//        System.out.println("Count After reading blight violations ="+count);
//        writeViolationsFile(blightViolations);
//
//    }

    private static void writeViolationsFile(List<BlightViolation> violations) throws IOException {
        final Object [] FILE_HEADER = {"buildingid","violationcode","date"};
        CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator(NEW_LINE_SEPARATOR);
        FileWriter fileWriter = new FileWriter(DIR+"violations-master.csv");
        //initialize CSVPrinter object
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord(FILE_HEADER);
        for(BlightViolation violation : violations){
            csvFilePrinter.printRecord(new Object[]{violation.getBuildingId(),violation.getCode(),violation.getDate()});
        }
        fileWriter.flush();
        fileWriter.close();
    }
    private static void write311File(List<Complain311> complain311s) throws IOException {
        final Object [] FILE_HEADER = {"buildingid","issue_type","date","ticket_status"};
        CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator(NEW_LINE_SEPARATOR);
        FileWriter fileWriter = new FileWriter(DIR+"master311.csv");
        //initialize CSVPrinter object
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord(FILE_HEADER);
        for(Complain311 complain311 : complain311s){
            csvFilePrinter.printRecord(new Object[]{complain311.getBuildingId(),complain311.getIssueType(),complain311.getTicketStatus(),complain311.getDate()});
        }
        fileWriter.flush();
        fileWriter.close();
    }

    public static void main(String[] args) throws IOException {
        String latlng= "(34.77852088100008, -86.60567625399995)";
        String loc = extractLocation(latlng);
        System.out.println(loc);

//        //blightViolations
//        createBVData();
//        //crimes
//        createCrimesData();
//        //311 data
//        create311Data();
//        //permits
//        createPermitsData();
//
//        List<Building> masterList = new ArrayList<Building>();
//        masterList.addAll(bvBuildings);
//        masterList.addAll(cBlds);
//        masterList.addAll(pBlds);
//        masterList.addAll(three11Blds);
//
//
//
//        System.out.println("Master list size " + masterList.size());
//        System.out.println("311 size" + three11Blds.size());
//        System.out.println("BVBlds " + bvBuildings.size());
//        System.out.println("cBilds size " + cBlds.size());
//        System.out.println("PBld size="+pBlds.size());
//
//        for(int i=0;i<masterList.size();i++){
//            masterList.get(i).setId(i+1);
//        }
//
//        writeOutput(masterList,"master-buildings.csv");



    }



    private static void createPermitsData() throws IOException {
        //createPermitBlds(bvBuildings.size()+cBlds.size()+three11Blds.size());

    }



    private static void create311Data() throws IOException {
        three11Blds = create311Buildings(bvBuildings.size()+cBlds.size());

    }

    private static List<Building> create311Buildings(int i) throws IOException {
        String t11file = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit311.csv";
        Reader reader = new FileReader(t11file);
        return get311Buildings(i,  reader);
    }
    private static List<Building> get311Buildings(int startCount, Reader reader) throws IOException {
        List<Building> privateBuildings = new ArrayList<Building>();
//        List<Complain311> t11Calls = new ArrayList<>();
//        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);
//        int count = startCount+2;
//        System.out.println("Start count at 311 " + count);
//
//        for(CSVRecord record : parser){
//            String address;
//            address = record.get("address");
//            address = address.toLowerCase();
//            if(address.indexOf("detroit")>0){
//                address = address.substring(0,address.indexOf("detroit")-1).trim();
//            }
//
//
//            try{
//
//                double latitude = Double.valueOf(record.get("lat"));
//                double longitude = Double.valueOf(record.get("lng"));
//                Building bld = new Building(count,latitude,longitude,address);
//                privateBuildings.add(bld);
//                count++;
//                Complain311 complain311 = new Complain311(record.get("issue_type"),record.get("ticket_status"),count,record.get("ticket_created_date_time"));
//                t11Calls.add(complain311);
//
//            }catch (NumberFormatException nfe){
//                //go on
//                //lets try location
//                String[] addresspieces = record.get("location").split("\n");
//                if(addresspieces.length<2 || address.isEmpty()){
//                    System.out.println("No way to reconcile for record Address=" + record.get("address")+":Location=" + record.get("location"));
//                    continue;
//
//                }
//                Building bld;
//                try{
//                    Pair<Double,Double> location = extractLocation(addresspieces[1]);
//                    bld = new Building(count,location.getLeft(),location.getRight(),address);
//
//                }catch (Exception e){
//                    bld = new Building(count,0.0,0.0,address);
//                }
//                privateBuildings.add(bld);
//                Complain311 complain311 = new Complain311(record.get("issue_type"),record.get("ticket_status"),count,record.get("ticket_created_date_time"));
//                t11Calls.add(complain311);
//                count++;
//
//            }
//
//
//        }
//        write311File(t11Calls);
//
//        System.out.println("End count at 311 " + count);
        return privateBuildings;
    }

    private static void createCrimesData() throws IOException {
        cBlds = createCrimeBuildings(bvBuildings.size());
        writeOutput(cBlds,"crime-buildings.csv");
    }

    private static void createBVData() throws IOException {
        //createBVBuildings(0);


    }

    private static List<Building> createCrimeBuildings(int startCount) throws IOException {

        String crimeFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit-crime.csv";
        Reader reader = new FileReader(crimeFile);
        return getCrimeBuildings(startCount,  reader);
    }

    private static List<Building> getCrimeBuildings(int startCount, Reader reader) throws IOException {
        List<Building> justBuildings = new ArrayList<Building>();
        List<Crime> crimes = new ArrayList<>();
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);
        int count = startCount+1;
        System.out.println("Count at start Crimes" + count);

//        for(CSVRecord record : parser){
//            String address;
//            try{
//
//                double latitude = Double.valueOf(record.get("LAT"));
//                double longitude = Double.valueOf(record.get("LON"));
//                address = record.get("ADDRESS").toLowerCase();
//                Building bld = new Building(count,latitude,longitude,address);
//                justBuildings.add(bld);
//                Crime crime = new Crime(record.get("INCIDENTDATE"),count,record.get("CATEGORY"));
//                crimes.add(crime);
//
//
//            }catch (NumberFormatException nfe){
//                //go on
//                //lets try location
//                address = record.get("ADDRESS").toLowerCase();
//                String[] addresspieces = record.get("LOCATION").split("\n");
//                if(addresspieces.length<2 || address.isEmpty()){
//                    //System.out.println("No way to reconcile for record Address=" + record.get("ADDRESS")+":Location=" + record.get("LOCATION"));
//
//                    continue;
//
//                }
//                Building bld;
//                try{
//                    Pair<Double,Double> location = extractLocation(addresspieces[1]);
//                    bld = new Building(count,location.getLeft(),location.getRight(),address);
//
//                }catch (Exception e){
//                    bld = new Building(count,0.0,0.0,address);
//                }
//                justBuildings.add(bld);
//                Crime crime = new Crime(record.get("INCIDENTDATE"),count,record.get("CATEGORY"));
//                crimes.add(crime);
//
//
//
//            }
//             count++;
//
//        }
        writeCrimesFile(crimes);
        System.out.println("Count at End Crimes" + count);

        return justBuildings;
    }

    private static void writeCrimesFile(List<Crime> crimes) throws IOException {
        final Object [] FILE_HEADER = {"buildingid","category","date"};
        CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator(NEW_LINE_SEPARATOR);
        FileWriter fileWriter = new FileWriter(DIR+"master-crimes.csv");
        //initialize CSVPrinter object
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord(FILE_HEADER);
        for(Crime crime : crimes){
            csvFilePrinter.printRecord(new Object[]{crime.getBuildingId(),crime.getCategory(),crime.getDate()});
        }
        fileWriter.flush();
        fileWriter.close();

    }

    private static void writeOutput(List<Building> bvBlds,String filename) throws IOException {
        //CSV file header
        final Object [] FILE_HEADER = {"buildingid","addressLine1","lat","lng","isBlighted"};
        CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator(NEW_LINE_SEPARATOR);
        FileWriter fileWriter = new FileWriter("/home/sajit/coursera/ds_scale/blightfight/course-data/"+filename);
        //initialize CSVPrinter object
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

        csvFilePrinter.printRecord(FILE_HEADER);
        for(Building building : bvBlds){
            int blightedInd = building.isBlighted()? 1 : 0;
            csvFilePrinter.printRecord(new Object[]{building.getId(),building.getAddress(),building.getLatitude(),building.getLongitude(),blightedInd});
        }
        fileWriter.flush();
        fileWriter.close();

    }

    public static String extractLocation(String addressPart) {
        int begIdx = addressPart.indexOf("(");
        int endIdx = addressPart.indexOf(")");
        if(begIdx<0 || endIdx <0){
            //System.out.println("Location = " + addressPart);
            throw new RuntimeException("bad address");
        }
        String[] parts = addressPart.substring(begIdx+1,endIdx).split(",");
        if(parts.length<2){
            throw new RuntimeException("bad address" + addressPart);
        }
        return "("+parts[0].trim()+":"+parts[1].trim()+")";

    }
}
