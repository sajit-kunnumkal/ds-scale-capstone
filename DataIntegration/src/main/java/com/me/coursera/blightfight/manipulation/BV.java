package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.BlightViolation;
import com.me.coursera.blightfight.models.Building;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajit on 6/29/16.
 */
public class BV {

    public static String HEADER = "b_id,location,category,date\n";
    public static void main(String[] args) throws IOException {

        String blightFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/detroit-blight-violations.csv";
        Reader reader = new FileReader(blightFile);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);
        int count = 1;
        List<String> output = new ArrayList<>();
        output.add(HEADER);

        for(CSVRecord record : parser){
            String[] addressParts = record.get("ViolationAddress").split("\n");
            try{
                String location = BVUtils.extractLocation(addressParts[2]);
                String violationCode = record.get("ViolationCode");
                String date = record.get("TicketIssuedDT");
                output.add(count+","+location+","+violationCode+","+date);
                count++;
            }catch (Exception e){
                //nothing to do
            }



        }

        File oFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/bv-data.csv");
        FileUtils.writeLines(oFile,output);

    }
}
