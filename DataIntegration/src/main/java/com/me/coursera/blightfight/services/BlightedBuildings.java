package com.me.coursera.blightfight.services;

import com.me.coursera.blightfight.manipulation.BVUtils;
import com.me.coursera.blightfight.models.Building;
import com.me.coursera.blightfight.models.Location;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.beans.Expression;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by sajit on 6/24/16.
 */
public class BlightedBuildings {

    public static void main(String[] args) throws IOException {

        Set<Location> dsmBlds = getDismantledBuildings().stream().map(aBuilding ->
                new Location(String.valueOf(aBuilding.getLatitude()),String.valueOf(aBuilding.getLongitude()))).collect(Collectors.toSet());


        String pfile = "/home/sajit/coursera/ds_scale/blightfight/course-data/master-buildings.csv";
        getMasterBuildings(pfile,dsmBlds);



    }

    private static void getMasterBuildings(String pfile,Set<Location> dsmLocales) throws IOException {
        File file = new File(pfile);
        List<String> lines = FileUtils.readLines(file);

        List<String> output = new ArrayList<>();
        output.add(lines.get(0));
        for(int i=1;i<lines.size();i++){
            String currentLine = lines.get(i);
            String[] keys = currentLine.split(",");
            assert keys.length == 5;
            System.out.println(currentLine);
            Location location = new Location(keys[2],keys[3]);
            int isBlighted = 0;
            if(dsmLocales.contains(location)){
                isBlighted = 1;
            }
            output.add(currentLine.substring(0,currentLine.length()-1)+isBlighted);
        }
        String xFile = "/home/sajit/coursera/ds_scale/blightfight/course-data/master-buildings2.csv";
        File oFile = new File(xFile);
        FileUtils.writeLines(oFile,output);
    }

    private static Set<Building> getDismantledBuildings() throws IOException {

        Set<Building> result = new HashSet<>();
        String pfile = "/home/sajit/coursera/ds_scale/blightfight/course-data/permits-alldata.csv";

        Reader reader = new FileReader(pfile);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);


        for (CSVRecord record : parser) {
            String permitType = record.get("BLD_PERMIT_TYPE");
            if (permitType.equals("Dismantle") || permitType.equals("DISM")) {
                String address = record.get("SITE_ADDRESS").toLowerCase();
                if (address.indexOf("detroit") > 0) {
                    address = address.substring(0, address.indexOf("detroit") - 1).trim();
                }


                String[] addresspieces = record.get("site_location").split("\n");
                if (addresspieces.length >= 3) {
                    //Pair<Double, Double> location = BVUtils.extractLocation(addresspieces[2]);
                    try {

                        //Building bld = new Building(location.getLeft(), location.getRight(), address);
                        //bld.setId(Long.valueOf(record.get("buildingid")));
                        //result.add(bld);
                        //System.out.println("Dismantled buildings " + bld.getId());


                    } catch (Exception e) {
                        //
                    }
                }
            }
        }
        parser.close();
        //System.out.println("Size of dism buildings " + result.size());
        return  result;

    }
}
