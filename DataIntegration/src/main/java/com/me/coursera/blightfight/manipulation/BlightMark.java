package com.me.coursera.blightfight.manipulation;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajit on 7/1/16.
 */
public class BlightMark {

    private static final String HEADER = "b_id,lat,lng,category,date,is_blighted";

    public static void main(String[] args) throws IOException {
        File file = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/concat.csv");
        List<String> lines = FileUtils.readLines(file);
        List<String> output = new ArrayList<>();
        output.add(BlightMark.HEADER);
        for(String line : lines){

            String[] tokens = line.split(",");
            String category = tokens[4];
            int isblighted = 0;
            if(category.equals("\"Dismantle\"") || category.equals("\"DISM\"")){
                isblighted = 1;
            }

            //System.out.println("category" + category + "is blighted " + isblighted);
            if(isblighted==1){
                System.out.println("IS DSM");
            }
            output.add(line+","+isblighted);
        }
        File oFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/bbld.csv");
        FileUtils.writeLines(oFile,output);
    }
}
