package com.me.coursera.blightfight.manipulation;

import com.me.coursera.blightfight.models.Location;
import com.me.coursera.blightfight.models.TrainingModel1;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by sajit on 7/4/16.
 */
public class PreProcessUtil {

    static Set<String> blightedLocales = new HashSet<>();
    public static void main(String[] args) throws IOException {
        File bvFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/bv-data.csv");
        List<String> bvLines = FileUtils.readLines(bvFile);
        Map<String,Integer> bvMap = counts(bvLines);
//
        File crimeFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/crime-data.csv");
        List<String> crimeLines = FileUtils.readLines(crimeFile);
        Map<String,Integer> crimeMap = counts(crimeLines);

        //311
        File t11File = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/t11-data.csv");
        List<String> t11Lines = FileUtils.readLines(t11File);
        Map<String,Integer> t11Map = counts(t11Lines);
        //Permits
        File permitsFile = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/permit-data.csv");
        List<String> permitLines = FileUtils.readLines(permitsFile);
        Map<String,Integer> permitMap = counts(permitLines);

        Map<String,TrainingModel1> dataset = createDataset(bvMap,crimeMap,t11Map,permitMap);
        System.out.println(dataset.size());


        //System.out.println("Going to calculate distances");
        Set<Location> allLocations = dataset.keySet().stream().map(aLocation -> Location.from(aLocation)).collect(Collectors.toSet());
        Set<Location> blighedLocations = blightedLocales.stream().map(blightedLocale -> Location.from(blightedLocale)).collect(Collectors.toSet());
        for(String location : dataset.keySet()){
              Location current = Location.from(location);
              double ratio = ModelTrain1.calculate(current,allLocations,blighedLocations);
              TrainingModel1 val = dataset.get(location);
               val.nnb = ratio;
                System.out.println("Location ="+location+":: nnb="+ratio);
               dataset.put(location,val);
        }
        File output = new File("/home/sajit/coursera/ds_scale/blightfight/course-data/output/t1.csv");
        List<String> outputLines = new ArrayList<>();
        outputLines.add("location,bv_count,crime_count,t11_count,bnn,is_blighted");
        for(String location : dataset.keySet()) {
            TrainingModel1 model = dataset.get(location);
            outputLines.add(location+","+model.bvCount+","+model.crimeCount+","+model.t11Count+","+model.nnb+","+model.isBlighted);
        }

        FileUtils.writeLines(output,outputLines);
    }

    private static Map<String,TrainingModel1> createDataset(Map<String, Integer> blightMap, Map<String, Integer> crimeMap, Map<String, Integer> t11Map, Map<String, Integer> permitMap) {
        Map<String,TrainingModel1> myMap = new HashMap<>();
        for(String location : blightMap.keySet()){
            int bvCount = blightMap.get(location);
            TrainingModel1 model1 = myMap.get(location);
            if(model1 == null){
                model1 = new TrainingModel1();
            }
            model1.bvCount = bvCount;
            myMap.put(location,model1);
        }

        for(String location : crimeMap.keySet()){
            int crimeCount = crimeMap.get(location);
            TrainingModel1 model1 = myMap.get(location);
            if(model1 == null){
                model1 = new TrainingModel1();
            }
            model1.crimeCount = crimeCount;
            myMap.put(location,model1);
        }

        for(String location : t11Map.keySet()){
            int t11Count = t11Map.get(location);
            TrainingModel1 model1 = myMap.get(location);
            if(model1 == null){
                model1 = new TrainingModel1();
            }
            model1.t11Count = t11Count;
            myMap.put(location,model1);
        }

        for(String location : permitMap.keySet()){
            int pCount =permitMap.get(location);
            TrainingModel1 model1 = myMap.get(location);
            if(model1 == null){
                model1 = new TrainingModel1();
            }
            model1.isBlighted = true;
            blightedLocales.add(location);
            myMap.put(location,model1);
        }

        return myMap;
    }

    private static Map<String,Integer> counts(List<String> records){
        Map<String,Integer> result = new HashMap<>();
        for(int i=1;i<records.size();i++){
            String record = records.get(i);
            //System.out.println(record);
            String[] tokens = record.split(",");
            String location = tokens[1];
            Integer count = result.get(location);
            if(count == null){
                count = 0;
            }
            count = count + 1;
            //System.out.println("Location = " + location);
            result.put(location,count);

        }

        return  result;
    }
}
