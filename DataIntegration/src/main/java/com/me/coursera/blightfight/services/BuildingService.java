package com.me.coursera.blightfight.services;

import com.me.coursera.blightfight.manipulation.BVUtils;
import com.me.coursera.blightfight.models.Building;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by sajit on 6/19/16.
 */
public class BuildingService {

    List<Building> allBuildings = new ArrayList<>();
    public BuildingService() throws IOException {
        String file = BVUtils.DIR+"master-buildings.csv";
        Reader reader = new FileReader(file);
        CSVParser parser = CSVFormat.EXCEL.withHeader().parse(reader);
        List<Building> masterList = new ArrayList<>();

        for(CSVRecord record : parser){
            Building bld = new Building(Long.valueOf(record.get("buildingid")),Double.valueOf(record.get("lat")),
                    Double.valueOf(record.get("lng")),record.get("addressLine1"));
            allBuildings.add(bld);

        }

    }

    public List<Building> getBuildingsByCoordinates(double lat,double lng){
        return allBuildings.stream().
                filter(building -> {
                    double threshold = 0.0001;
                    double absDiffLat = Math.abs(building.getLatitude()-lat);
                    double absDiffLng = Math.abs(building.getLongitude()-lng);
                    return threshold > absDiffLat && threshold > absDiffLng;
                }).collect(Collectors.toList());
    }

    public static void main(String[] args) throws IOException {
        BuildingService service = new BuildingService();
        //double lat = 42.42032, lng =  -83.14533;
        double lat =42.35531, lng = -83.2175;
        List<Building> result = service.getBuildingsByCoordinates(lat,lng);
        for(Building building : result){
            System.out.println(building);
        }
    }
}
