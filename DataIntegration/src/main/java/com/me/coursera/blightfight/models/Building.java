package com.me.coursera.blightfight.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sajit on 6/11/16.
 */
public class Building {
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        if (Double.compare(building.latitude, latitude) != 0) return false;
        return Double.compare(building.longitude, longitude) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    private double latitude;
    private double longitude;
    private String address;
    private long id;
    private boolean isBlighted = false;

    public void setBlighted(boolean isBlighted){
        this.isBlighted = isBlighted;
    }

    public boolean isBlighted(){
        return  isBlighted;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Building(long id,double latitude,double longitude,String address){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    public Building(double latitude,double longitude,String address){
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString(){
        return "{id:"+id+",lat:"+latitude+",lng:"+longitude+",addr:"+address+"}";
    }

    public static void main(String[] args){
        Building b1 = new Building(1,0.44,3.5955,"dss");
        Building b2 = new Building(2,0.44,3.5955,"dsdf");
        System.out.println(b1.equals(b2));
        Set<Building> buildingSet = new HashSet<>();
        buildingSet.add(b1);
        System.out.print(buildingSet.contains(b2));
    }
}
