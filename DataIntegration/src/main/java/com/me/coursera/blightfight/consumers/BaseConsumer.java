package com.me.coursera.blightfight.consumers;

import com.socrata.api.Soda2Consumer;
import com.socrata.exceptions.LongRunningQueryException;
import com.socrata.exceptions.SodaError;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.core.MediaType;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by sajit on 6/11/16.
 */
public class BaseConsumer {

    protected  final String username = System.getenv("username");
    protected final String password = System.getenv("password");
    protected final String token = System.getenv("token");

    protected final Soda2Consumer consumer;
    protected final String url;

    public BaseConsumer(String url){
        this.url = url;
        consumer = Soda2Consumer.newConsumer(url,
                username, password, token);
    }

    public void writeFile(String filename) throws IOException, URISyntaxException, LongRunningQueryException, SodaError {
        URI uri = new URI(url);
        InputStream inputStream = consumer.getHttpLowLevel().queryRaw(uri, MediaType.TEXT_PLAIN_TYPE).getEntityInputStream();
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        String theString = writer.toString();
        FileWriter fileWriter = new FileWriter(filename);
        fileWriter.write(theString);
        fileWriter.close();
    }

}
